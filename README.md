# SALVAGE DB

### Using
1. Execute `composer install` on the project root dir.

2. Transfer `salvage-db` dir to your document root path.

3. Access to your endpoint url. (e.g. http://example.com/salvage-db/)
  - If displayed **error occurred.**, that successes transfer.

4. Excute below like command.

``` bash
curl --silent
--anyauth --user ${SALVAGE_HTTP_AUTH_USER}:${SALVAGE_HTTP_AUTH_PASSWORD}
-d "host=${SALVAGE_DB_HOST}"
-d "port=${SALVAGE_DB_PORT}"
-d "databaseName=${SALVAGE_DB_NAME}"
-d "user=${SALVAGE_DB_USER}"
-d "pass=${SALVAGE_DB_PASSWORD}"
-X POST ${SALVAGE_HTTP_END_POINT}/index.php
```

5. After step 4, so execute below like command.

``` bash
curl --silent
--anyauth --user ${SALVAGE_HTTP_AUTH_USER}:${SALVAGE_HTTP_AUTH_PASSWORD}
-X GET ${SALVAGE_HTTP_END_POINT}/${SALVAGE_DB_NAME}.sql.gz
```

6. OK, completed. Salvaged your database. :D