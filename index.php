<?php
ini_set( 'max_execution_time' , 0);
require_once dirname(__FILE__) . '/vendor/autoload.php';

$host = '';
$port = '';
$databaseName = '';
$user = '';
$pass = '';

if (isset($_POST['host'])) {
    $host = $_POST['host'];
}
if (isset($_POST['port'])) {
    $port = $_POST['port'];
}
if (isset($_POST['databaseName'])) {
    $databaseName = $_POST['databaseName'];
}
if (isset($_POST['user'])) {
    $user = $_POST['user'];
}
if (isset($_POST['pass'])) {
    $pass = $_POST['pass'];
}

// https://github.com/eaudeweb/mysqldump-php#constructor-and-default-parameters
$dumpSettings = array(
    'compress' => Ifsnop\Mysqldump\Mysqldump::GZIP,
    'add-locks' => true,
    'single-transaction' => true
);

try {
    $dsn = sprintf(
        'mysql:host=%s;port=%s;dbname=%s',
        $host,
        $port,
        $databaseName
    );
    $dump = new Ifsnop\Mysqldump\Mysqldump($dsn, $user, $pass, $dumpSettings);
    $dump->start($databaseName . '.sql.gz');
} catch (\Exception $e) {
    echo 'Error occurred. Please contact https://gitlab.com/solutus-llc/salvage-db/-/issues';
}
